import React from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Button, Row, Col, Container } from 'reactstrap';
import classnames from 'classnames';
import { Link } from 'react-router-dom';


export default class BookDetail extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1',
      book: {}
    };
  }

  toggle(tab) {
    if (this.state.activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  componentDidMount(){
    fetch(`https://reduxblog.herokuapp.com/api/posts/${this.props.match.params.id}?key=818828`)
    .then(response=>response.json())
    .then(data=>{
        this.setState(state=>({
            book: data
        }))
        console.log(this.state.project)
    })
  }

  render() {
    return (
      <Container className="mt-4 bg-light">

        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              Book Info
            </NavLink>
          </NavItem>
          
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              ...
            </NavLink>
          </NavItem>  
        </Nav>

        <TabContent activeTab={this.state.activeTab} className="mt-5 mb-4">  
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <h4 className="mb-3">Title:  {this.state.book.title}</h4>
                <p>Author:  {this.state.book.categories}</p>
                <p>Description:  {this.state.book.content}</p>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
          	<p className="mt-5 mb-4">The maintainer of this site is still trying to figure out on why did he put this tab...</p>
          </TabPane>
        </TabContent>

        <Link to="/booklist">
          <Button>Back</Button>
        </Link>
        
      </Container>
    );
  }
}