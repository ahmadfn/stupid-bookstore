import React from 'react';
import { Table, Container, Row, Button } from 'reactstrap';
import { Link } from 'react-router-dom';


export default class AddBookForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      bookArr: []
    }
  }

  componentDidMount = () => {
    fetch('https://reduxblog.herokuapp.com/api/posts?key=818828')
      .then(response => response.json())
      .then(data =>{
          /*
          if (this.state.bookArr[0] === null) {
            this.setState(state => ({
              bookArr: [...data]
            }));
          }
          */
          this.setState(state => ({
              bookArr: [...state.bookArr, ...data]
          }))
      })
  }

  render() {
    return (
      <Container className="pl-0 pr-0">
        <Table>
          <thead>
            <tr>
              <th>id</th>
              <th>Book Name</th>
              <th>Author</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {this.state.bookArr.map(book => {
              return (
                <tr key={book.id}>
                  <th scope="row">{book.id}</th>
                  <td><Link to={`/booklist/${book.id}`}>{book.title}</Link></td>
                  <td>{book.categories}</td>
                  <td>{book.content}</td>
                </tr>
              );
            })
          }
          </tbody>
        </Table>

      </Container>
    );
  }
}