import React from 'react';
import { Button, Form, FormGroup, Label, Input, Container } from 'reactstrap';
import { Redirect, Link } from 'react-router-dom';
import './style.css';


export default class Example extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      title: '',
      author: '',
      description: '',
      isRedirected: false
    }
  }

  handleChangeBookName = (event) => {
    this.setState({
      title: event.target.value
    });
  }

  handleChangeAuthorName = event => {
    this.setState({
      author: event.target.value
    });
  }

  handleChangeBookDesc = event => {
    this.setState({
      description: event.target.value
    });
  }

  handleClickSubmit = () => {
    fetch("https://reduxblog.herokuapp.com/api/posts?key=818828",
        {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, cors, *same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json',
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify({
            title: this.state.title,
            categories: this.state.author,
            content: this.state.description
        })
        
    })
      .then(() => {
        this.setState({
          isRedirected: true
        });
      })
  }

  render() {
    if (this.state.isRedirected) {
      return (
        <Redirect to="/booklist"></Redirect>
      );
    }
    return (
      <Container className="pl-0 pr-0">
        <Form className="bg-azure p-3 mt-3">
          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Label for="bookName">Book Name</Label>
            <Input onChange={this.handleChangeBookName} type="text" name="bookName" id="bookName" placeholder="Enter book name here" />
          </FormGroup>
          
          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Label for="authorName">Author Name</Label>
            <Input onChange={this.handleChangeAuthorName} type="text" name="authorName" id="authorName" placeholder="Enter author name here" />
          </FormGroup>
          
          <FormGroup className="mb-3 mt-3 mr-sm-2 mb-sm-0">
            <Label for="bookDesc" className="mr-sm-2">Book Description</Label>
            <Input onChange={this.handleChangeBookDesc} type="textarea" name="bookDesc" id="bookDesc" placeholder="Enter book description here" />
          </FormGroup>
          
          <Button color="primary" onClick={this.handleClickSubmit} className="mt-3 mb-3 mr-2">Submit</Button>

          <Link to="/">
            <Button className="ml-2">Back</Button>
          </Link>
        </Form>
      </Container>
    );
  }
}