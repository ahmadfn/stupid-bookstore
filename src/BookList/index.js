import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col, Button } from 'reactstrap';
import BookTable from './BookTable';
import AddBookForm from './AddBookForm';
import './style.css';


export default class BookList extends React.Component {
  render() {
    return (
      <Container className="bg-azure mt-3 rounded-lg">
      	<Row className="pt-3 pb-3">
          <Link to="/addbook">
            <Button className="mr-2 ml-3" color="primary">Add book</Button>
          </Link>

          <Link to="/">
            <Button className="ml-2">Back</Button>
          </Link>
        </Row>
        <Row>
			<BookTable></BookTable>
      	</Row>
      </Container>
    );
  }
}