import React from 'react';
//import './App.css';
import BookList from './BookList/';
import LandingPage from './LandingPage/';
import AddBookForm from './BookList/AddBookForm.js';
import BookDetail from './BookDetail/';
import Nav from './common/Nav';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";


function App() {
  return (
    <Router>
      <div className="App">
          <Nav></Nav>
          <Switch>
            <Route path="/" exact component={LandingPage}></Route>
            <Route path="/booklist" exact component={BookList}></Route>
            <Route path='/booklist/:id' component={BookDetail} ></Route>
            <Route path="/addbook" component={AddBookForm}></Route>
          </Switch>
      </div>
    </Router>

  );
}

export default App;
