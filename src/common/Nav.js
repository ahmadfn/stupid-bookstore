import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'reactstrap';
import './commonStyle.css';


export default class Nav extends React.Component {
  render() {
    return (
      <Container>
        <Row className="nav-bar rounded">
          <Col xl="4" lg="4" md="4" sm="4" xs="4">
            <ul className="mb-0">
              <li><Link to="/">Home</Link></li>
            </ul>
          </Col>
          <Col xl="8" lg="8" md="8" sm="8" xs="8">
            <ul className="flex-end mb-0">
              <li className="pl-2 pr-3">
                <Link to="/booklist">Books List</Link>
              </li>
              <li className="pl-2 pr-3 mr-3">
                <Link to="/addbook">Add book</Link>
              </li>
            </ul>
          </Col>
        </Row>
      </Container>
    );
  }
}