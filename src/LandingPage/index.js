import React from 'react';
import { Container, Row } from 'reactstrap';
import LandingText from './Jumbotron';


export default class LandingPage extends React.Component {
  render() {
    return (
      <Container>
        <Row>
         	<LandingText></LandingText>
        </Row>
      </Container>
    );
  }
}