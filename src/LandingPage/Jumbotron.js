import React from 'react';
import { Jumbotron } from 'reactstrap';
import './landingStyle.css';


const LandingText = (props) => {
  return (
    <div className="width-100">
      <Jumbotron fluid className="pr-5 pl-5">
        <h1 className="display-3">Stupid Bookstore</h1>
        <p className="lead">An enlightening book? Sounds boring. In here, we have tons of books to lower your IQ level</p>
        <p>Become stupid people has never been this easy!</p>
      </Jumbotron>
    </div>
  );
};

export default LandingText;